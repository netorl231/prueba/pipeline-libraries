import static org.fmbbva.movcli.pipeline.Utilities.*
import groovy.json.JsonOutput

// vars/gitPushMasterAndTag.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    //now build, based on the configuration provided
    def userEmail = "${config.userEmail}"
    def versionR = getVersionToRelease("${env.BRANCH_NAME}")   
    def changelogNotes = ""
    
    node {
		
		// Extacción de comentarios del fichero changelog en la rama de release
		if (fileExists('changelog')) {
			changelogNotes = extractMergeCommitMessage(readFile('changelog'))
		}
		
		// Commits de los cambios que pueda haber hecho Jenkinsfile en la rama de release
		sh "git -c user.email=${userEmail} commit -am 'Cambio de version' || echo '[WARN] Commit failed. There is probably nothing to commit.'"   
		
		// Cambio a la rama de Master
		sh "git -c user.email=${userEmail} checkout master"
		
		// Merge de la rama de release con Master
		def mergeMessage = "Merge branch ${env.BRANCH_NAME} into master\n\n" + changelogNotes;	
		echo "mergeMessage: ${mergeMessage}"
		sh "git -c user.email=${userEmail} merge --no-ff ${env.BRANCH_NAME} -m '${mergeMessage}'"
				
		// Crear Tags a partir de master
		sh "git -c user.email=${userEmail} tag -a ${versionR} -m 'Version ${versionR}'" 
		
		// Subir al repositorio remoto los cambios de Master y la etiqueta generada
		sh "git -c user.email=${userEmail} push origin master"				
		sh "git -c user.email=${userEmail} push origin --tags"
		
		// Cambio a la rama de release
		sh "git -c user.email=${userEmail} checkout ${env.BRANCH_NAME}"
    }
	
}
