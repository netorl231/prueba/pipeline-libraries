// vars/sonarQualityGate.groovy
def call(body) {

	// evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    node {
		
		echo "${env.SONAR_HOST_URL}"
		
		ws("tmp/${env.JOB_NAME}") {
			
			  waitUntil {
			  
				sh "curl ${env.SONAR_CE_TASK_URL} -o ceTask.json"
				
				def status = sh (
					script: "cat ceTask.json | jq --raw-output '.task.status' | tr -d '\n'",
					returnStdout: true
				)
				
				return "SUCCESS".equals(status)
			  }
			
			def analysisId = sh (
				script: "cat ceTask.json | jq --raw-output '.task.analysisId' | tr -d '\n'",
				returnStdout: true
			)
				
			def qualityGateUrl = "${env.SONAR_HOST_URL}/api/qualitygates/project_status?analysisId=" + analysisId
			
			sh "curl $qualityGateUrl -o qualityGate.json"
			
			def qualitygateStatus = sh (
				script: "cat qualityGate.json | jq --raw-output '.projectStatus.status' | tr -d '\n'",
				returnStdout: true
			)
			
			sh "cat qualityGate.json"									
						
			if ("ERROR".equals(qualitygateStatus) && !config["skipOnError"]) {
			  error  "Quality Gate failure"
			}			

			echo "Quality gates with status: ${qualitygateStatus}"
		}
		
	}
	
}
