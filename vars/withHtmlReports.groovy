import static org.fmbbva.movcli.pipeline.Utilities.*

// vars/withHtmlReports.groovy
def call(dir, files,  Closure body) {
   
	node {
					
		try {
		
			body()
								
		} finally {
		
			publishHTML (target: [alwaysLinkToLastBuild: true, reportDir: "${dir}",reportFiles: "${files}",reportName: "Informe de tests"])
		}
	}
	
}
