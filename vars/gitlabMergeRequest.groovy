import static org.fmbbva.movcli.pipeline.Utilities.*
import groovy.json.JsonOutput

// vars/gitlabMergeRequest.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    //now build, based on the configuration provided
    node {
		
		def versionR = getVersionToRelease("${env.BRANCH_NAME}")
		def payload = JsonOutput.toJson([
            source_branch: "${env.BRANCH_NAME}",
            target_branch: "${config.targetBranch}",
            title: "Automatic Merge Request for release ${versionR}"
		])


		def idProject = sh (
			script: "curl --silent -k -H \'PRIVATE-TOKEN: QJy3mrjr9WuHpNg-yUhZ\' ${config.urlApi}/${config.idProject} | jq 'select((.name == \"${config.nameProject}\") and (.namespace .name == \"${config.groupProject}\")).id' | tr -d '\n'",
			returnStdout: true
		)
				
		sh "curl -i -k -H \'PRIVATE-TOKEN: QJy3mrjr9WuHpNg-yUhZ\' -H \'Content-Type:application/json\' -X POST -d \'${payload}\' ${config.urlApi}/${idProject}/merge_requests | tee mr_output";
		
		
		MR = sh (
			script: "cat mr_output | grep 'HTTP/1.1 201 Created'",
			returnStatus: true
		)
		
		if (MR != 0) {
			error("Error al crear el Merge Request a la rama ${config.targetBranch}")			
		}

	}
	
}
