// vars/deployApkOnNexus.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    //now build, based on the configuration provided
    node {
			
		// Calcular la version
		def apkVersion = sh (
			script: "cat package.json | jq --raw-output '.version' | tr -d '\n'",
			returnStdout: true
		)
		
		sh "/usr/bin/mvn deploy:deploy-file -B -DgroupId=${config.groupId} -DartifactId=${config.artifactId} -Dversion=${apkVersion} -Dfile=${env.APK_FILE} -DrepositoryId=nexus.releases -Durl=${config.registry} -DgeneratePom=true"
	    
	}
	
}
