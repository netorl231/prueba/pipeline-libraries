// vars/checkoutBranch.groovy
def call(body) {

	// evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    node {
		
		git url: "${config.url}"								
			
		def remoteBranch = sh(returnStdout: true, script: "for branch in `git branch -r | grep -v HEAD`;do echo -e `git show --format='%ci' \$branch | head -n 1` \$branch; done | sort -r | head -n 1 | awk '{print \$4}'").trim()
		def remoteRepo = sh(returnStdout: true, script: "git remote show").trim()
		
		checkout([
			$class: 'GitSCM', 
			branches: [[name: "${remoteBranch}"]], 
			doGenerateSubmoduleConfigurations: false, 
			extensions: [], 
			submoduleCfg: [], 
			userRemoteConfigs: [[credentialsId: '476a5c22-df5e-4fa4-9db4-04a17837e3bf', url: "${config.url}"]]]
		)
			
		env.BRANCH_NAME =  "${remoteBranch}".substring("${remoteRepo}".length()+1)
		
		echo "BRANCH to BUILD: ${env.BRANCH_NAME}"
		
	}
	
}
