import static org.fmbbva.movcli.pipeline.Utilities.*
import groovy.json.JsonOutput

// vars/performRelease.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    //now build, based on the configuration provided
    def userEmail = "${config.userEmail}"
    def releaseVersion = getVersionToRelease("${env.BRANCH_NAME}")
    def changelogNotes = ""
    def nextVersion = ""
    node {

  		// Extacción de comentarios del fichero changelog en la rama de release
  		if (fileExists('changelog')) {
  			changelogNotes = extractMergeCommitMessage(readFile('changelog'))
  		}

  		// Cambio a la rama de release
  		sh "git -c user.email=${userEmail} checkout ${env.BRANCH_NAME}"
  		sh "git checkout master"
  		sh "git checkout develop"
  		sh "git flow init -f -d"
  		sh "git flow release finish -m 'release-version: ${releaseVersion}' ${releaseVersion}-RELEASE "

  		nextVersion = nextSnaphotVersion("${releaseVersion}")

  		// Cambio de version develop
  		mvn this, "${env.WORKSPACE}", "versions:set -DnewVersion=${nextVersion} versions:commit"
  		sh "git -c user.email=${userEmail} commit -am 'Cambio de version develop ${nextVersion}' || echo '[WARN] Commit failed. There is probably nothing to commit.'"
  		sh "git -c user.email=${userEmail} push origin develop"

  		// Cambio a la rama de Master
  		sh "git -c user.email=${userEmail} checkout master"

  		// Subir al repositorio remoto los cambios de Master y la etiqueta generada
  		sh "git -c user.email=${userEmail} push origin master"
  		sh "git -c user.email=${userEmail} push origin --tags"
      sh "git push origin --delete ${env.BRANCH_NAME}"

    }

}
