import static org.fmbbva.movcli.pipeline.Utilities.*
import groovy.json.JsonOutput

// vars/performRelease.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    //now build, based on the configuration provided
    def credentialsId = "${config.credentialsId}"
    def changelogNotes = ""

	  def releaseVersion = []
  	def didTimeout = false
  	try {
  			timeout(time: 45, unit: 'SECONDS') { // change to a convenient timeout for you
          releaseVersion = input message: 'Ready to release?', parameters: [string(defaultValue: '', description: 'Escriba la version para la release con formato X.Y.Z', name: 'releaseVersion')]
  			}
  	} catch(err) { // timeout reached or input false
  			didTimeout = true
  			echo "[prepareRelease][ERROR] Aborted ->: [${err}]"
  	}

   	echo "Esperando a que se de la aprovacion para creación de release"
  	node {

        try {
    			if (didTimeout) {
    					echo "[prepareRelease] no input was received before timeout. release aborted"
    			} else if (releaseVersion.size() > 0) {
    				echo "branch ${env.BRANCH_NAME}"
    				withCredentials([usernamePassword(credentialsId: credentialsId, passwordVariable: 'password', usernameVariable: 'username')]) {
    					echo "[prepareRelease] release parameters -> releaseVersion=${releaseVersion}"
    					sh "git branch"
    					sh "git checkout master"
    					sh "git checkout develop"
    					sh "git flow init -f -d"
    					sh "git flow release start ${releaseVersion}"
    					echo "release started ${releaseVersion}"
    					mvn this, "${env.WORKSPACE}", "versions:set -DnewVersion=${releaseVersion} versions:commit"
    					sh "git add -A && git commit -am 'update versions for release ${releaseVersion}'"
    					echo "updated versions for release ${releaseVersion}"
    					sh "git flow release publish ${releaseVersion}"
    					echo "[prepareRelease] branch ${env.BRANCH_NAME}"
    				}
    			} else {
    				echo "release parameter required"
    				currentBuild.result = 'FAILURE'
    			}
      } catch (err) {
          echo "[prepareRelease][ERROR]this was not successful"
  	    	echo "[prepareRelease][ERROR] Caught: ${err}"
  	    	currentBuild.result = 'FAILURE'
  		}
  }
}
