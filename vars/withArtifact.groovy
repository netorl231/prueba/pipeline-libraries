// vars/withArtifact.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
		
	def provisionFile = "provision"
	
	if(config["name"] != null) {
		provisionFile = "${config.name}"
	}
	
	if(config["gzip"] != null) {
		sh "tar -cvzf ${provisionFile}.tar.gz -C ${env.WORKSPACE_JOB}/${config.gzip} ."
	}
	
	if(config["file"] != null) {
		sh "cp ${env.WORKSPACE_JOB}/${config.file} ."
	}
	
}
