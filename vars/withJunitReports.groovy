import static org.fmbbva.movcli.pipeline.Utilities.*

// vars/withJunitReports.groovy
def call(body) {

	node {
		
		try {
				
			body()
									
		} finally {
		
			junit '**/target/surefire-reports/TEST-*.xml'
			step([$class: 'JacocoPublisher'])
		
		}
	}
	
}
