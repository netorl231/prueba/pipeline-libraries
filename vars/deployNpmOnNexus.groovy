import static org.fmbbva.movcli.pipeline.Utilities.*

// vars/deployNpmOnNexus.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    //now build, based on the configuration provided
    node {
		npm this, "publish --registry ${config.registry}"
	}
	
}
