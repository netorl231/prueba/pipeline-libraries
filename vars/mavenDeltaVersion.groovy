import static org.fmbbva.movcli.pipeline.Utilities.*

// vars/mavenDeltaVersion.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    // now build, based on the configuration provided		
	node {
	
		def newVersion
		if (config.mode == "release") {
			newVersion = getVersionToRelease("${env.BRANCH_NAME}")
		} else if (config.mode == "next-snapshot") {
			newVersion = nextSnaphotVersion(getVersionToRelease("${env.BRANCH_NAME}"))			
		}
		
		if(newVersion){			
			try{
				mvn this, "${env.WORKSPACE}", "versions:set -DnewVersion=${newVersion}"
				mvn this, "${env.WORKSPACE}", "versions:commit"
				echo "Nueva versión: ${newVersion}"
			} catch (err) {				
				mvn this, "${env.WORKSPACE}", "versions:revert"
			}
			
		}
		
	}
	
	
	
	
}

