// vars/signApk.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    node {
		
		def keystore = "/var/lib/jenkins/keystores/${config.alias}.keystore"
		
		//Generar la clave RSA.
		if (! fileExists("${keystore}")) {
			sh "keytool -genkey -noprompt -alias ${config.alias} -dname \"${config.dname}\" -keystore ${keystore} -storepass ${config.storepass} -keypass ${config.keypass} -keyalg RSA -validity 10000 -keysize 2048"
		}
		
		def unsignedApkFile = sh (
			script: "basename platforms/android/build/outputs/apk/*.apk | tr -d '\n'",
			returnStdout: true
		)
		
		//Firmar el Apk
		sh "jarsigner -verbose -storepass ${config.storepass} -sigalg SHA1withRSA -digestalg SHA1 -keystore ${keystore} platforms/android/build/outputs/apk/${unsignedApkFile} ${config.alias}"
		
		// Ultima versión disponible de Android SDK
		def androidSdkVersion = sh (
			script: "ls -t  ${env.ANDROID_HOME}/build-tools | head -1 | tr -d '\n'",
			returnStdout: true
		)
		
		// Optimización de Apk
		sh "${env.ANDROID_HOME}/build-tools/${androidSdkVersion}/zipalign -v 4 platforms/android/build/outputs/apk/${unsignedApkFile} ${config.alias}.apk"
		
		env.APK_FILE = "${config.alias}.apk"
		
	}
}
