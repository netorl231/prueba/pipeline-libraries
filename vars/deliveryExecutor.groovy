import org.fmbbva.movcli.pipeline.Deliverable

def run (deliveries, branch, script) {

	node {
		env.WORKSPACE_JOB = "${env.WORKSPACE}"
	}

	println deliveries
	
	def Deliverable delivery
	for (it in deliveries) {
	
		delivery = it as Deliverable
		println delivery
		
		if (delivery.matchStrategy(branch)) {
			delivery.execute(script)
		}
	
	}

}


