import static org.fmbbva.movcli.pipeline.Utilities.*

// vars/withJunitAndCucumberReports.groovy
def call(body) {

	node {
	
		body()
		
		try {
		
			junit '**/target/surefire-reports/TEST-*.xml'
			step([$class: 'JacocoPublisher'])
			step($class: 'CucumberTestResultArchiver', testResults: '**/target/cucumber.json')
			step([$class: 'CucumberReportPublisher', failedFeaturesNumber: 0, failedScenariosNumber: 0, failedStepsNumber: 0, fileExcludePattern: '', fileIncludePattern: '**/*.json',
                             jsonReportDirectory: '', parallelTesting: false, pendingStepsNumber: 0, skippedStepsNumber: 0, trendsLimit: 0, undefinedStepsNumber: 0])
		} catch (error) {
		
			sh "[WARNING] Some reports failed"
		
		}
	}
	
}
