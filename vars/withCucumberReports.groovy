import static org.fmbbva.movcli.pipeline.Utilities.*

// vars/withCucumberReports.groovy
def call(body) {

	node {
					
		try {
				
			body()
						
		} finally {
		
			step($class: 'CucumberTestResultArchiver', testResults: '**/target/cucumber.json')
			step([$class: 'CucumberReportPublisher', failedFeaturesNumber: 0, failedScenariosNumber: 0, failedStepsNumber: 0, fileExcludePattern: '', fileIncludePattern: '**/*.json',
                             jsonReportDirectory: '', parallelTesting: false, pendingStepsNumber: 0, skippedStepsNumber: 0, trendsLimit: 0, undefinedStepsNumber: 0])
		
		}
	}
	
}
