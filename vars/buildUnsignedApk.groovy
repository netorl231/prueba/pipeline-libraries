// vars/buildUnsignedApk.groovy
def call(body) {

    // evaluate the body block, and collect configuration into the object
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    
    node {
        sh "sudo npm install -g cordova"
    
		sh "ionic state reset"
		sh "cordova platform add android@latest --save || echo '[WARN] Platform android not added . There is probably already added.'"
		
		if(config["buildResources"]) {
			sh "ionic resources"
		}
		
		sh "ionic build android --release"
		
	}
}
