import static org.fmbbva.movcli.pipeline.Utilities.*

// vars/withNotification.groovy
def call(script, Closure body) {

    try {
    
		loadContext(script)
        body()
        
    } finally {
    
		node {
			emailext (
				  subject: "STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
				  body: """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
					<p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>""",
				  recipientProviders: [[$class: 'DevelopersRecipientProvider']]
				)
		}
    }
    
}
