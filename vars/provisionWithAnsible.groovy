// vars/provisionWithAnsible3.groovy
def call(String rol, String inventory, Closure body) {
    
    node {
		
		echo "ENTORNO _____ ${env.WORKSPACE_JOB}"
		
		ws("tmp/${env.JOB_NAME}") {		
			
			echo "ENTORNO _____ ${env.WORKSPACE}"
			
			sh "rm -rf *"	
			//checkout([$class: 'GitSCM', branches: [[name: '*/develop']], userRemoteConfigs: [[url: 'git@54.76.148.112:movcli-arq-infra/ansible-roles.git']]])
			checkout([$class: 'GitSCM', branches: [[name: '*/develop']], userRemoteConfigs: [[url: 'https://gitlab.com/netorl231/prueba/ansible-roles.git']]])
							
			sh "cp -f ${env.WORKSPACE_JOB}/cdelivery/provision/hosts ."
			sh "cp -f ${env.WORKSPACE_JOB}/cdelivery/provision/site.yml ."
			sh "cp -rf ${env.WORKSPACE_JOB}/cdelivery/provision/vars roles/${rol}"
					
			body()
			
			
			sh "echo Execute playbooks ansible"
			
			def branch = "${env.BRANCH_NAME}".replace("/","-")		
			
			//sh "ansible-playbook -b -i hosts --extra-vars 'inventory=${inventory} branchName=${branch}' --verbose site.yml"
			
			sh "ansible-playbook -b -i hosts --extra-vars 'inventory=${inventory} branchName=${branch}' host_key_checking = False --verbose site.yml"

			//sh "ansible-playbook -s -i hosts --extra-vars 'inventory=${inventory} branchName=${branch}' --verbose site.yml
													
			//sh "ansible-playbook -s -i hosts --extra-vars inventory=${inventory} --verbose site.yml"								

		}
	}
	
}
