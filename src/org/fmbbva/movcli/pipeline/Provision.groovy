package org.fmbbva.movcli.pipeline;

	def getBranchStrategy(branchName) {
	
		def strategy = null;
		if (env.BRANCH_NAME.contains("/")) {
			strategy  = branchName.substring(0, env.BRANCH_NAME.lastIndexOf("/"));
		} else {
			strategy  = branchName;
		}

		return strategy;
	}
