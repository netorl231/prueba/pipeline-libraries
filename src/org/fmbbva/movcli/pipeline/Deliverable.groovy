package org.fmbbva.movcli.pipeline

public interface Deliverable {                                         
    
    public abstract void execute(script);
	public abstract boolean matchStrategy(branch);
		                 
}
