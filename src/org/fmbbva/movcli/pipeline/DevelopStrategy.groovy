package org.fmbbva.movcli.pipeline

public class DevelopStrategy implements Deliverable, Serializable  {

		public void execute(script) {  
			
			try {
				script.developStrategy(script).call()
				
			} catch (java.lang.NoSuchMethodError err) {
				script.echo "Aborted ->: [${err}]"
				script.stage ('Develop delivery') {
					script.node {
						script.echo 'Implemente en su Jenkinsfile un método "developStrategy" describiendo la estrategia de entrega para la rama de develop:\n\ndef developStrategy(script) {\n   return {\n   	//Aquí su estrategia de entrega\n   }\n}'
					}	
				}
				
			}
		}
		
		public boolean matchStrategy(branch) {
			return branch.contains("develop")
		}
}
