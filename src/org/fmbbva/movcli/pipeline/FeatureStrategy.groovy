package org.fmbbva.movcli.pipeline

public class FeatureStrategy implements Deliverable, Serializable  {

	public void execute(script) {  
	
		try {
			script.featureStrategy(script).call()
			
		} catch (java.lang.NoSuchMethodError err) {
			
			script.stage ('Feature delivery') {
				script.node {
					script.echo 'Implemente en su Jenkinsfile un método "featureStrategy" describiendo la estrategia de entrega para ramas feature/*:\n\ndef featureStrategy(script) {\n   return {\n   	//Aquí su estrategia de entrega\n   }\n}'
				}	
			}
			
		} 						
		
	}
	
	public boolean matchStrategy(branch) {
		return branch.contains("feature")
	}
}
