package org.fmbbva.movcli.pipeline

class Utilities {
  
	static def mvn(script, buildDir, args) {
		script.sh "/usr/bin/mvn -f ${buildDir}/pom.xml ${args}"
	}
	
	static def npm(script, args) {
		script.sh "/usr/bin/npm ${args}"
	}
	
	static def getVersionToRelease(branch) {
		def matcher = branch =~ "release/([0-9]+.[0-9]+.[0-9]+)"
		return matcher ? matcher[0][1] : null	
	}
	
	static def nextSnaphotVersion(versionReleased) {
		def matcher = versionReleased =~ "([0-9]+).([0-9]+).([0-9]+)"
		def mayor = matcher[0][1]
		def minor = (matcher[0][2] as Integer) + 1
				
		return mayor + "." + minor + ".0-SNAPSHOT"
	}
	
	static def getVersionFromPom(pom) {
		def matcher = pom =~ "<version>(.+)</version>"
		return matcher ? matcher[0][1] : null	
	}
	
	static def nextSnaphotVersionFromPom(pom) {
		def matcher = pom =~ "<version>([0-9]+).([0-9]+).([0-9]+)</version>"
		def mayor = matcher[0][1]
		def minor = (matcher[0][2] as Integer) + 1
				
		return mayor + "." + minor + ".0-SNAPSHOT"
	}
	
	static def extractMergeCommitMessage(changelog) {	
				
		def message = "";
		def matcher;
		if ((matcher = changelog =~ /(?s)(# \d+.\d+.\d+ \[\d+-\d+-\d+\]\n)(.*?)(\n+)(#.*|$)/)) {
		  message = matcher.group(2);
		}
		
		return message;
	}

	static def loadContext(script) {
	
		script.env.SONAR_HOST_URL = "http://54.76.148.112:9000/sonar"
		script.env.ANDROID_HOME = "/data/android-sdk"
		script.env.SONAR_SCANNER_HOME = "/var/lib/sonar-plugins/sonar-scanner/sonar-scanner-2.8"
	}

	static def Properties getProperty(script, filename, property) {
		def properties = new Properties()
		properties.load(new StringReader(script.readFile(filename)))
		return properties.getProperty(property)
	}
	
}
