Librerías compartidas para la entrega continua
==============================================

## 1. Introducción
Colección de utilidades para la automatización del proceso de entrega continua (tubería o pipeline)


## 2. Jenkinsfile
Cada proyecto definirá su proceso de entrega continua escribiendo el fichero de **Jenkinsfile**. 
Este fichero implementa la tubería para el proceso de entrega continua en función de la rama del repositorio (branchName) que dispare el proceso. Cada acción de `push` sobre el repositorio de **Gitlab** lanzará la ejecución de un `job multibranch` configurado en **Jenkins** parar cada proyecto.

### 2.1 Estructura general
La estructura general de Jenkinsfile presenta una acción `withNotificationAndClean` que es reponsable de ejecutar la tubería completa, enviar una notificación por correo y limpiar el workspace al finalizar.
El proceso ejecutado variará en función de la rama del repositorio que desencadenó la construcción en **Jenkins**.

```data
#!/usr/bin/env groovy

@Library('pipeline@master')
import static org.fmbbva.movcli.pipeline.Utilities.*
import org.fmbbva.movcli.pipeline.*


withNotificationAndClean(this) {

	stage ('Checkout sources') {
		node {
			git branch: "${env.BRANCH_NAME}", url: 'git@54.76.148.112:movcli-back/pilotofc.git'
		}
	}

	// Estrategía de entrega según la rama
	deliveryExecutor.run([new DevelopStrategy(),new ReleaseStrategy(),new FeatureStrategy()], env.BRANCH_NAME, this)

}

def featureStrategy(script) {

}


def developStrategy(script){

}


def releaseStrategy(script) {

}
```

### 2.2 Estrategias para la entrega continua
Se definien tres estrategias para la automatización de los proccesos de entrega continua.

1. Estrategía de Feature
La estrategia de feature es implementada por la función `featureStrategy` definida en el Jenkinsfile. Es frecuente que albergue procesos de construcción, pruebas unitarias y de integración, y despliegue en entornos de desarrollo.

```data
def featureStrategy(script) {

}

```

2. Estrategía de Develop
La estrategia de develop es implementada por la función `developStrategy` definida en el Jenkinsfile. Se definen procesos de construcción, pruebas unitarias y de integración, así como análisis de métricas de calidad y despliegue en entornos de desarrollo.

```data
def developStrategy(script) {

}

```


3. Estrategía de Release
La estrategia de release es implementada por la función `releaseStrategy` definida en el Jenkinsfile. Además de los procesos definidos en las estrategias anteriores, la tubería de release verifica que se cumplen con las metricas de calidad definidas en el **servidor de SonarQube**, y despliega el componenete a un entorno de preproducción (**Stage**). Cuando son satisfechas todos los requisitos funcionales y no funcionales, se produce el despliegue automático en el entorno de producción; y finalmente se integran los cambios en las ramas master y develop del repositorio.

```data
def releaseStrategy(script) {

}

```


## 3. DSLs 
Jenkinsfile permite escribir las tuberias -asociadas a los procesos de entrega- en forma de **Scripting** haciendo uso de DSLs basados en el lenguaje **Groovy**.
Estos DSLs -**Domain specific languajes**- son subdominios de lenguaje que permiten definir abstracciones más apropiadas a los procesos de entrega automatizados por la tubería. A continuación se presentan los DSLs disponibles para escribir el fichero de **Jenkinsfile**.


### 3.1 buildUnsignedApk
Construye un archivo APK -con la aplicación móvil- sin firmar.

#### 3.1.1 Parametros
- `buildResources`
Indica si tiene que construir los recursos (imagenes, etc) antes de crear el APK.


```data
buildUnsignedApk {		
	buildResources = true				
}
```

### 3.2 deployApkOnNexus
Despliega el archivo APK en el repositorio Nexus; utiliza un repositorio de tipo maven para mantener el versionado del artefacto.


#### 3.2.1 Parametros
- `groupId`
Identificador para el grupo del aartefacto
- `artifactId`
Identificador para el artefacto
- `registry`
URL para el repositorio de Nexus

```data
deployApkOnNexus {
   groupId = "org.fmbbva.movcli"
   artifactId = "pilotofc"
   registry = "http://54.76.148.112:8081/nexus/repository/mobile-fmbbva/"			           
}
```

### 3.3 deployNpmOnNexus
Despliega un `modulo javascript` en el repositorio Nexus.

#### 3.3.1 Parametros
- `registry`
URL del repositorio Nexus.

```data
deployNpmOnNexus {
   registry = "http://54.76.148.112:8081/nexus/repository/npm-fmbbva/"
}
```

### 3.4 gitlabMergeRequest
Integra los cambios con la rama `targetBranch` del repositorio de código, a través de un merge request.

#### 3.4.1 Parametros

- `urlApi`
URL del API expuesta por **Gitlab**
- `idProject`
Identificador del proyecto en el repositorio de código.
- `nameProject`
Nombre del proyecto en el repositorio de código.
- `groupProject`
Nombre del grupo al que pertenece el proyecto.
- `targetBranch`
Rama con la que se integran los cambios.

```data
gitlabMergeRequest {
	urlApi = "https://54.76.148.112:7443/gitlab/api/v3/projects"
	idProject = "77"
	nameProject = "pilotofc"
	groupProject = "movcli-back"
	targetBranch = "develop"
}
```

### 3.5 gitPushMasterAndTag
Integra los cambios con la rama `master` del repositorio de código

#### 3.5.1 Parametros
- `userEmail`
Correo del usuario que realiza la integración.


```data
gitPushMasterAndTag {
	userEmail = "jenkins@fmbbva.es"
}
```

### 3.6 mavenDeltaVersion
Actualiza la versión del POM (Project object model).


#### 3.6.1 Parametros
- `mode`
Establece el algoritmo para cambiar de versión. Puede ser `release` o `snapshot`

```data
mavenDeltaVersion {
	mode = 'release'
}
```

### 3.7 provisionWithAnsible
Provisiona un artefacto indicando el Rol de Ansible y el inventario de máquinas sobre el que lanzarlo.

#### 3.7.1 Parametros
- `rol`
Rol de ansible para provisionar el artefacto.
- `inventario`
Inventario de máquinas.


```data
provisionWithAnsible('jar-service', 'pre-n1') {

	withArtifact {
	  file = "piloto-web/target/*.jar"
	}
	
}
```

### 3.8 signApk
Firma el archivo APK.

#### 3.8.1 Parametros
- `alias`
Alias de la clave/certificado
- `dname`
Identifica la entidad asociada a la clave.
- `storepass`
Contraseña para el almacen de claves.
- `keypass`
Contraseña para la clave/certificado.

```data
signApk {							
	alias = "pilotofc"
	dname = "CN=pilotofc, OU=MF, O=BBVA, L=Madrid, S=Madrid, C=ES"
	storepass = "piloto"
	keypass = "piloto"
}
```

### 3.9 sonarQualityGate
Verifica que el análisis del proyecto superó con éxito las metricas de calidad definidas en el servidor de SonarQube.

#### 3.9.1 Parametros
- `skipOnError`
Determina si la tubería continua o no en caso de no superar el quality gate.

```data
sonarQualityGate {
	skipOnError = false
}
```

### 3.10 withArtifact
Copia un artefacto al directorio actual.

#### 3.10.1 Parametros
- `name`
Nombre del artefacto que se va a copiar a partir de un directorio
- `gzip`
Carpeta o directorio a comprimir.
- `file`
Archivo para copiar


```data
withArtifact {
  file = "piloto-web/target/*.jar"
}
```

```data
withArtifact {
  gzip = "www"
  name = "pilotofc"
}	
```

### 3.11 withCucumberReports
Crea enlaces a los resultados de los tests Cucumber ejecutados.

```data
withCucumberReports {			
	mvn this, "${env.WORKSPACE}", 'verify -DfinalName=piloto -Dspringboot-layout=MODULE -Dskip.surefire.tests=true -Dpmd.skip=true -Dcheckstyle.skip=true'
}
```

### 3.12 withHtmlReports
Crea enlaces a los informes Html generados.

#### 3.12.1 Parametros
- `dir`
Directorio de los informes generados.
- `files`
Archivo a enlazar.


```data
withHtmlReports("test/result/unit", "unit.html") {
  sh "xvfb-run npm test"
}
```

### 3.13 withJunitAndCucumberReports
Crea enlaces a los resultados de tests Junit && Cucumber ejecutados.


```data
withJunitAndCucumberReports {
    mvn this, "${env.WORKSPACE}", 'clean package -Dpmd.skip=true -Dcheckstyle.skip=true -Dskip.surefire.tests=false'			
	mvn this, "${env.WORKSPACE}", 'verify -DfinalName=piloto -Dspringboot-layout=MODULE -Dskip.surefire.tests=true -Dpmd.skip=true -Dcheckstyle.skip=true'
}
```

### 3.14 withJunitReports
Crea enlaces a los resultados de tests Junit ejecutados.

#### 3.14.1 Parametros

```data
withJunitReports {						
	mvn this, "${env.WORKSPACE}", 'clean package -Dpmd.skip=true -Dcheckstyle.skip=true -Dskip.surefire.tests=false'
}
```

### 3.15 withNotification
Ejecuta un `body` notificando el resultado.

#### 3.15.1 Parametros
- `script`
Objeto que representa el script en ejecución.

```data
withNotification(this) {

	stage ('Checkout sources') {
		node {
			git branch: "${env.BRANCH_NAME}", url: 'git@54.76.148.112:movcli-back/pilotofc.git'
		}
	}

	// Estrategía de entrega según la rama
	deliveryExecutor.run([new DevelopStrategy(),new ReleaseStrategy(),new FeatureStrategy()], env.BRANCH_NAME, this)

}
```

### 3.16 withNotificationAndClean
Ejecuta un `body` notificando el resultado y limpiando el workspace asociado al final de la ejecución.

#### 3.116.1 Parametros
- `script`
Objeto que representa el script en ejecución.

```data
withNotificationAndClean(this) {

	stage ('Checkout sources') {
		node {
			git branch: "${env.BRANCH_NAME}", url: 'git@54.76.148.112:movcli-front/proyecto-base-angular-ionic.git'
		}
	}
	
	// Estrategía de entrega según la rama
	deliveryExecutor.run([new DevelopStrategy(),new ReleaseStrategy(),new FeatureStrategy()], env.BRANCH_NAME, this)
	
}
```




## 4. Referencias

- [Scripted pipelines](https://jenkins.io/doc/book/pipeline/jenkinsfile/#advanced-scripted-pipeline)
